var gulp = require('gulp');
var newer = require('gulp-newer');

// Config
var config = require('../config').datas;

// Tool
var browsersync = require('../tools/browsersync');

// task
gulp.task('datas', function(){
    return gulp.src(config.src)
        .pipe(newer(config.dest))
        .pipe(gulp.dest(config.dest))
        .pipe(browsersync.reload({stream: true}));
});