import Mockup from './Mockup';

class SliderMockups {

    constructor(el){
        this.el = el;
        this.items = [];
        this.max = 0;
    };

    init(url) {
        console.log('ici 2');
        //définition du conteneur du contenu
        this.container = this.el.querySelector('.slider_wrapper');

        //Chargement du json
        this.load(url);
    };

    load(url) {
        const req = new XMLHttpRequest();
        //overture du fichier json
        req.open('GET', url, true);
        //renvoi req sur la fonction loaded pour verifier s'il est chargé
        req.addEventListener('readystatechange', this.loaded.bind(this, req));
        req.send();
    };

    loaded(req) {
        if (req.readyState === 4) {
            if (req.status === 200) {
                //après vérifiation du chargement du fichier on traite les données
                this.build(JSON.parse(req.responseText));
            }
        }
    };

    build(datas) {
        //nombre max d'objets
        this.max = datas.length;

        let i = 0;
        let item;

        for (i; i < this.max; i++) {
            //construction des slides
            item = new Mockup(i, datas[i]);
            this.container.appendChild(item.build());
            this.items.push(item);
        }
    };
}
export default SliderMockups;
