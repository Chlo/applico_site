class Mockup{
    constructor(id, datas){
        this.id = id;
        this.datas = datas;
    };

    build() {
        this.el = document.createElement('div');
        this.el.classList.add("slides");

        console.log('ici 3');

        // Process datas
        this.el.classList.add("mockups-" + this.id);
        this.el.innerHTML = '' +
            '<div><img class="mockups-app" src="' + this.datas.url + '"/></div>';

        return this.el;
    };
}

export default Mockup;