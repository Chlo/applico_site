class Translation {

    constructor() {
        //this.lang = this.getCookie('lang');
    }

    // Content - LOAD JSON
    init(lang) {

        if (document.getElementById('projet')) {
            this.load('./datas/cqqcoqp.json', 'cqqcoqp', lang);
            this.load('./datas/progression.json', 'progression', lang);
            this.load('./datas/siteProjet.json', 'siteProjet', lang);
        } else if (document.getElementById('equipe')) {
            this.load('./datas/membre.json', 'membre', lang);
            this.load('./datas/siteEquipe.json', 'siteEquipe', lang);
        }
    };

    load(url, type, lang) {
        let requestURL = url;
        let request = new XMLHttpRequest();
        request.open('GET', requestURL);
        request.responseType = 'json';
        request.send();

        request.onload = function () {
            let response = request.response;
            let translation = new Translation();

            if (type === "cqqcoqp") translation.showTradCqqcoqp(response, lang);
            if (type === "membre") translation.showTradMembre(response, lang);
            if (type === "progression") translation.showTradProgression(response, lang);
            if (type === "siteProjet") translation.showTradSiteProjet(response, lang);
            if (type === "siteEquipe") translation.showTradSiteEquipe(response, lang);
        };
    };

    // Content - CQQCOQP
    showTradCqqcoqp(jsonObj, lang) {
        document.querySelector('.cqqcoqp').innerHTML = "";
        for (var i = 0; i < jsonObj.length; i++) {

            document.querySelector('.cqqcoqp').innerHTML += "" +
                "<div class='" + jsonObj[i].class + " col-md-3 offset-md-2'>" +
                "<img src='" + jsonObj[i].img + "'>" +
                "<h2>" + jsonObj[i].title + "</h2>" +
                "<p>" + jsonObj[i][lang].description + "</p>" +
                "</div>";
        }
    };

    // Content - Membres
    showTradMembre(jsonObj, lang) {
        document.querySelector('.profiles').innerHTML = "";
        for (var i = 0; i < jsonObj.length; i++) {

            document.querySelector('.profiles').innerHTML += "" +
                "<article class='profile " + jsonObj[i].class + "'>" +
                "<div class='profile-img'></div>" +
                "<h2 class='profile-name'>" + jsonObj[i].name + "</h2>" +
                "<p class='profile-role'>" + jsonObj[i][lang].role + "</p>" +
                "<p class='profile-detail'>" + jsonObj[i][lang].description + "</p>";
        }
    };

    // Content - Progress bar
    showTradProgression(jsonObj, lang) {
        for (var i = 0; i < jsonObj.length; i++) {
            if (document.querySelector('.timeline .progression').classList.contains('nb-'+jsonObj[i].nb)) {
                document.querySelector('.timeline .progression h2').innerHTML = jsonObj[i][lang].title;
                document.querySelector('.timeline .progression .progression-bar--content').innerHTML = "<p class='col-sm-10 col-md-8 col-lg-5 " + jsonObj[i].nb + "'>" + jsonObj[i][lang].description + "</p>";
            }
        }
    };

    // Content - Page equite
    showTradSiteEquipe(jsonObj, lang) {
        document.querySelector('.header-nav-projet p').innerHTML = jsonObj[0][lang].nav_projet;
        document.querySelector('.header-nav-equipe p').innerHTML = jsonObj[0][lang].nav_equipe;
        document.querySelector('.equipe h1').innerHTML = jsonObj[0][lang].title;
        document.querySelector('.equipe .lalib').innerHTML = jsonObj[0][lang].text;
    };

    // Content - Page projet
    showTradSiteProjet(jsonObj, lang) {
        document.querySelector('.header-nav-projet p').innerHTML = jsonObj[0][lang].nav_projet;
        document.querySelector('.header-nav-equipe p').innerHTML = jsonObj[0][lang].nav_equipe;
        document.querySelector('.projet .pitch p').innerHTML = jsonObj[0][lang].pitch;
        document.querySelector('.projet .progression h1').innerHTML = jsonObj[0][lang].progression;
        document.querySelector('.projet .mockups p').innerHTML = jsonObj[0][lang].txtMockups;
        document.querySelector('.projet .apocobi h1').innerHTML = jsonObj[0][lang].apocobi.title;
        document.querySelector('.projet .apocobi .bulleapo').innerHTML = jsonObj[0][lang].apocobi.apo;
        document.querySelector('.projet .apocobi .bullecobi').innerHTML = jsonObj[0][lang].apocobi.cobi;
        document.querySelector('.projet .soon p').innerHTML = jsonObj[0][lang].soon;
    };

}

export default Translation;