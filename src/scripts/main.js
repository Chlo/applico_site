import Parallax from 'parallax-js';
//import SliderMockups from './components/SliderMockups';
import Translation from './lang';



document.onreadystatechange = () => {
    if (document.readyState === 'interactive') {
        // document ready

        // SLIDER Mockups
        /*if (document.querySelector(".slider-mockups")) {
            let slider = new SliderMockups(document.querySelector(".slider-mockups"));
            slider.init('./datas/interfaces.json');
        }*/

        // NAV
        document.querySelector('.header-nav-projet').onclick = function () {
            document.querySelector('.corps').style.right = "0";
            document.querySelector('.corps').style.transitionDuration = "250ms";
            document.querySelector('.header-nav-equipe').classList.remove('active');
            this.classList.add('active');
        };
        document.querySelector('.header-nav-equipe').onclick = function () {
            document.querySelector('.corps').style.right = "100%";
            document.querySelector('.corps').style.transitionDuration = "250ms";
            document.querySelector('.header-nav-projet').classList.remove('active');
            this.classList.add('active');
        };

        // LANG
        var lang = "";
        if (getCookie('lang') === '') {
            lang = "fr";
            document.cookie = "lang=" + lang;
            document.querySelector('.lang-'+lang).classList.add('active');

        } else if (getCookie('lang') === "fr"){
            document.querySelector('.lang-lang').classList.remove('active');
            document.querySelector('.lang-fr').classList.add('active');
            lang = "fr";
        } else if (getCookie('lang') === "en"){
            document.querySelector('.lang-lang').classList.remove('active');
            document.querySelector('.lang-en').classList.add('active');
            lang = "en";
        }

        var translation = new Translation();
        translation.init(lang);

        // NAV Lang
        document.querySelectorAll('.lang-lang').forEach(function (e) {

            e.onclick = function () {
                if (e.classList.contains('lang-fr')) {
                    document.querySelector('.lang-en').classList.remove('active');
                    lang = "fr";

                } else {
                    document.querySelector('.lang-fr').classList.remove('active');
                    lang = "en";
                }
                e.classList.add('active');
                document.cookie = "lang=" + lang;
                translation.init(lang);

                console.log('ici '+ getCookie('lang'));
            };
        });

        //PARALLAX
        if (document.querySelector('.fond')) {
            let fond = document.querySelector('.fond');
            let parallaxInstanceFond = new Parallax(fond, {
                relativeInput: true,
            });
        }

        //Get COOKIE by name
        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) === '') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) === 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        };

    }
};