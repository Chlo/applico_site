Ceci est le site de présentation pour le projet Applico.

Projet conceptualisé et développé par les étudiants en "Bachelor designer et développeur Intractif" des Gobelins et de la CCI formation de la Haute savoie.

__

This is the presentation website for the project Applico.

Project designed and developed by the students in "Bachelor designer et dévelopeur Intractif" from the Gobelins and the CCI formation of Haute savoie.


INITIALISATION
--

01 | Placer le projet à la racine d'un dossier vide - Put the projcet in an empty folder

02 | Ouvrir le projet téléchargé dans un IDE - Open the downloded projet in an IDE

03 | Télécharger les dépendances - Download the dependences : `npm i`

04 | Compiler le projet - Build the project : `npm start`


Production - https://www.applico.chloe-david.fr